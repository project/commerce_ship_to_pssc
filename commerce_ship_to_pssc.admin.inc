<?php

/**
 * @file
 * Settings functionality for the Commerce Ship To PSSC module.
 */

/**
 * Settings form.
 */
function commerce_ship_to_pssc_settings_form($form, &$form_state) {
  $form['options'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#title' => t('Miscellaneous options'),
    // '#description' => t(''),
    '#tree' => FALSE,
  );

  $form['options']['commerce_ship_to_pssc_status'] = array(
    '#title' => t('Change order status after export'),
    '#type' => 'select',
    '#options' => array(
      'none' => t('Do not change status'),
    ),
    '#description' => t('It is possible to automatically change the status of any order after it is exported. Care must be given to ensure that orders are in the correct status before the export is triggered.'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_ship_to_pssc_status', 'none'),
  );
  foreach (commerce_order_statuses() as $status_name => $status) {
    $form['options']['commerce_ship_to_pssc_status']['#options'][$status_name] = $status['title'];
  }

  $form['header'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#title' => t('Header options'),
    '#description' => t('If these options are left blank it is assumed they will be provided in the header of the view.'),
    '#tree' => FALSE,
  );
  $form['header']['commerce_ship_to_pssc_fromparty'] = array(
    '#type' => 'textfield',
    '#title' => t('From Party'),
    '#default_value' => variable_get('commerce_ship_to_pssc_fromparty'),
    '#description' => t('If this is not set here it must be provided in the header section of the view as a field that has the label "FromParty".'),
    '#required' => FALSE,
  );
  $form['header']['commerce_ship_to_pssc_custno'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer Number'),
    '#default_value' => variable_get('commerce_ship_to_pssc_custno'),
    '#description' => t('If this is not set here it must be provided in the header section of the view as a field that has the label "CustNo".'),
    '#required' => FALSE,
  );

  // Shipping options from the Flat Rate module.
  if (module_exists('commerce_flat_rate')) {
    $form['shipping_flatrate'] = array(
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      '#title' => t('Shipping codes and accounts: Flat rate'),
      '#description' => t('All shipping methods on this site must be mapped to a shipping code for PSSC; it is recommended to delete shipping options that are no longer needed. The account fields are optional.'),
      '#tree' => FALSE,
    );

    $form['shipping_flatrate']['map'] = array(
      '#theme' => 'commerce_ship_to_pssc_form_table',
      '#tree' => FALSE,
      '#header'=> array(
        t('Enabled'),
        t('Service'),
        t('PSSC shipping code'),
        t('PSSC shipping account'),
      ),
      '#empty' => t('No flat rate shipping options found.'),
      'rows' => array(
        '#tree' => FALSE,
      ),
    );

    foreach (commerce_flat_rate_commerce_shipping_service_info() as $service => $details) {
      $varname = 'commerce_ship_to_pssc_' . $service;
      $var_acct = $varname . '_acct';
      $var_code = $varname . '_code';
      $form['shipping_flatrate']['map']['rows'][$service] = array(
        array(
          '#title' => t('Status of %title shipping option', array('%title' => $details['display_title'])),
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#default_value' => TRUE,
          '#disabled' => TRUE,
        ),
        array(
          '#type' => 'markup',
          '#markup' => $details['display_title'],
        ),
        $var_code => array(
          '#title' => t('PSSC shipping code for %title', array('%title' => $details['display_title'])),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#default_value' => variable_get($var_code),
          '#size' => 5,
          '#maxlength' => 10,
        ),
        $var_acct => array(
          '#title' => t('PSSC shipping account for %title', array('%title' => $details['title'])),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#default_value' => variable_get($var_acct),
          '#size' => 5,
          '#maxlength' => 10,
        ),
      );
    }
  }

  // Shipping options from the USPS module.
  if (module_exists('commerce_usps')) {
    $form['shipping_usps'] = array(
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      '#title' => t('Shipping codes and accounts: USPS'),
      '#description' => t('All shipping methods on this site must be mapped to a shipping code for PSSC; it is recommended to disable shipping options that are no longer needed. The account fields are optional.'),
      '#tree' => FALSE,
    );

    $form['shipping_usps']['map'] = array(
      '#theme' => 'commerce_ship_to_pssc_form_table',
      '#tree' => FALSE,
      '#header'=> array(
        t('Enabled'),
        t('Service'),
        t('PSSC shipping code'),
        t('PSSC shipping account'),
      ),
      '#empty' => t('No USPS shipping options found.'),
      'rows' => array(
        '#tree' => FALSE,
      ),
    );

    $enabled = array_merge(variable_get('commerce_usps_services', array()), variable_get('commerce_usps_services_int', array()));
    foreach (commerce_usps_service_list() as $category => $services) {
      foreach ($services as $service => $details) {
        $varname = 'commerce_ship_to_pssc_' . $service;
        $var_acct = $varname . '_acct';
        $var_code = $varname . '_code';
        $var_enabled = $category . ':' . $service;
        $form['shipping_usps']['map']['rows'][$service] = array(
          $var_enabled => array(
            '#title' => t('Status of %title shipping option', array('%title' => $details['title'])),
            '#title_display' => 'invisible',
            '#type' => 'checkbox',
            '#default_value' => !empty($enabled[$service]),
          ),
          array(
            '#type' => 'markup',
            '#markup' => ucwords($category) . ': ' . $details['title'],
          ),
          $var_code => array(
            '#title' => t('PSSC shipping code for %title', array('%title' => $details['title'])),
            '#title_display' => 'invisible',
            '#type' => 'textfield',
            '#default_value' => variable_get($var_code),
            '#size' => 5,
            '#maxlength' => 10,
          ),
          $var_acct => array(
            '#title' => t('PSSC shipping account for %title', array('%title' => $details['title'])),
            '#title_display' => 'invisible',
            '#type' => 'textfield',
            '#default_value' => variable_get($var_acct),
            '#size' => 5,
            '#maxlength' => 10,
          ),
        );
      }
    }
  }

  // Shippable products.
  $form['products'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#title' => t('Product SKUs'),
    '#description' => t('All products require the SKU.'),
    '#tree' => FALSE,
  );

  $form['products']['map'] = array(
    '#theme' => 'commerce_ship_to_pssc_form_table',
    '#tree' => FALSE,
    '#header'=> array(
      t('Product'),
      t('SKU'),
      t('PSSC code'),
    ),
    '#empty' => t('No shippable products found.'),
    'rows' => array(
      '#tree' => FALSE,
    ),
  );

  $args = array('type' => 'product', 'status' => TRUE);
  foreach (commerce_product_load_multiple(array(), $args) as $product) {
    $varname = 'commerce_ship_to_pssc_product_' . $product->sku;
    $form['products']['map']['rows'][$product->sku] = array(
      array(
        '#type' => 'markup',
        '#markup' => $product->title,
      ),
      array(
        '#type' => 'markup',
        '#markup' => $product->sku,
      ),
      $varname => array(
        '#title' => t('PSSC product code for %title', array('%title' => $product->title)),
        '#title_display' => 'invisible',
        '#type' => 'textfield',
        '#default_value' => variable_get($varname),
        '#size' => 5,
        '#maxlength' => 10,
      ),
    );
  }

  // Submit button.
  $form['actions'] = array();
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Form API submission callback for commerce_ship_to_pssc_settings_form().
 */
function commerce_ship_to_pssc_settings_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  // Basic settings.
  variable_set('commerce_ship_to_pssc_fromparty', $values['commerce_ship_to_pssc_fromparty']);
  variable_set('commerce_ship_to_pssc_custno', $values['commerce_ship_to_pssc_custno']);
  variable_set('commerce_ship_to_pssc_status', $values['commerce_ship_to_pssc_status']);

  // Flat rate.
  if (module_exists('commerce_flat_rate')) {
    foreach (commerce_flat_rate_commerce_shipping_service_info() as $service => $details) {
      // If a code was stored for this service, save it as a variable.
      $var_code = 'commerce_ship_to_pssc_' . $service . '_code';
      if (isset($values[$var_code])) {
        variable_set($var_code, $values[$var_code]);
      }
      else {
        variable_set($var_code, '');
      }

      // If an account was stored for this service, save it as a variable.
      $var_acct = 'commerce_ship_to_pssc_' . $service . '_acct';
      if (isset($values[$var_acct])) {
        variable_set($var_acct, $values[$var_acct]);
      }
      else {
        variable_set($var_acct, '');
      }
    }
  }

  // USPS.
  if (module_exists('commerce_usps')) {
    // Update the services details.
    $old_services = array_merge(variable_get('commerce_usps_services', array()), variable_get('commerce_usps_services_int', array()));
    $new_services = array();
    $services_changed = FALSE;
    foreach (commerce_usps_service_list() as $category => $services) {
      foreach ($services as $service => $details) {
        // If a code was stored for this service, save it as a variable.
        $var_code = 'commerce_ship_to_pssc_' . $service . '_code';
        if (isset($values[$var_code])) {
          variable_set($var_code, $values[$var_code]);
        }
        else {
          variable_set($var_code, '');
        }

        // Work out if this service was disabled.
        $var_enabled = $category . ':' . $service;
        if (!empty($values[$var_enabled])) {
          $new_services[$category][$service] = $service;
        }
        else {
          $new_services[$category][$service] = 0;
        }

        // If a code was stored for this service, save it as a variable.
        $var_acct = 'commerce_ship_to_pssc_' . $service . '_acct';
        if (isset($values[$var_acct])) {
          variable_set($var_acct, $values[$var_acct]);
        }
        else {
          variable_set($var_acct, '');
        }

        // If the new value is different to the old value, note this for later.
        if (!isset($old_services[$service]) || $old_services[$service] !== $new_services[$category][$service]) {
          $services_changed = TRUE;
        }
      }
    }

    // Update the variables.
    variable_set('commerce_usps_services', $new_services['domestic']);
    variable_set('commerce_usps_services_int', $new_services['international']);

    // If the selected services have changed then rebuild caches.
    if ($services_changed) {
      commerce_shipping_services_reset();
      entity_defaults_rebuild();
      rules_clear_cache(TRUE);
      menu_rebuild();
    }
  }

  // Save the product mapping.
  $args = array('type' => 'product', 'status' => TRUE);
  foreach (commerce_product_load_multiple(array(), $args) as $product) {
    $varname = 'commerce_ship_to_pssc_product_' . $product->sku;
    if (isset($values[$varname])) {
      variable_set($varname, $values[$varname]);
    }
    else {
      variable_set($varname, '');
    }
  }
}

/**
 * Theme callback for the form table.
 */
function theme_commerce_ship_to_pssc_form_table(&$variables) {
  // Get the useful values.
  $form = $variables['form'];
  $rows = $form['rows'];
  $header = $form['#header'];
  $empty = isset($form['#empty']) ? $form['#empty'] : '';

  // Setup the structure to be rendered and returned.
  $content = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array(),
    '#empty' => $empty,
  );

  // Traverse each row.  @see element_chidren().
  foreach (element_children($rows) as $row_index) {
    $row = array();
    // Traverse each column in the row.  @see element_children().
    foreach (element_children($rows[$row_index]) as $col_index) {
      // Render the column form element.
      $row[] = drupal_render($rows[$row_index][$col_index]);
    }
    // Add the row to the table.
    $content['#rows'][] = $row;
  }

  // Render the table and return.
  return drupal_render($content);
}
