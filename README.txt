Commerce Ship to PSSC
---------------------
This module provides a method to export an XML file of order shipments in a
format required by PSSC.


Features
--------------------------------------------------------------------------------
The primary features include:

* Makes a custom path available to output Commerce orders in the XML format
  required by PSSC.

* Provides a settings page for customizing certain options.


Requirements
--------------------------------------------------------------------------------
The latest releases of the following are required:
* Commerce
* Commerce Shipping
* Views
* Views Bulk Operations
* Views Data Export


Installation
--------------------------------------------------------------------------------
 1. Configure the module as needed:
      admin/commerce/config/shipping/pssc

 2. Modify the Orders page for Commerce, which by default is at:
      admin/structure/views/view/commerce_orders

 3. Click the "Add" button on the Fields section, then click on "Bulk
    operations: Commerce Order" in the list; click "Apply (this display)".

 4. In the field settings dialog, enable the option named "Pass ids as arguments
    to a page"; set the "URL" option to:
      admin/commerce/orders/pssc/
    Please note that the trailing slash must be included. Enable the "Override
    label" option and change it to:
      Export to PSSC
    Click the "Apply (this display)" when finished.

 5. Click the "Save" button on the view.


Usage
--------------------------------------------------------------------------------
 1. Load the Orders admin page: admin/commerce/orders

 2. Select the orders to transmit to PSSC.

 3. Click the "Bulk operations" selector underneath the order list, select
    "Export to PSSC", then click "Apply".

 4. On the confirmation page, verify that the listed orders are the correct ones
    and click "Confirm" to start the export.


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Damien McKenna [1], development sponsored by the Lama
Yeshe Web Archive [2].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/commerce_ship_to_pssc


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: https://www.lamayeshe.com
